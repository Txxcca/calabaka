import telebot
from telebot import types
from bs4 import BeautifulSoup
import requests
from tok import token


link = 'https://random.dog/'
bot=telebot.TeleBot(token)
def findImage():
	response = requests.get(link)
	soup = BeautifulSoup(response.text, 'html.parser')
	if soup.find('video'):
		return 'Это видео, а не картинка сорри'
	img = soup.find(id='dog-img')['src']
	return (f'{link}/{img}')

@bot.message_handler(commands=['start'])
def start(message):
	markup = types.ReplyKeyboardMarkup(resize_keyboard=True)
	btn1 = types.KeyboardButton("Пес")
	markup.add(btn1)
	bot.send_message(message.chat.id, text="пес".format(message.from_user), reply_markup=markup)
@bot.message_handler(content_types=['text'])
def func(message):
	if(message.text == "Пес"):
		bot.send_message(message.chat.id, text=findImage())
	else:
		bot.send_message(message.chat.id, text="На такую комманду я не запрограммировал..")
bot.polling(none_stop=True)

